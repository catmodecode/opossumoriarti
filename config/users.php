<?php

return [
    'sasha' => env('SASHA'),
    'elina' => env('ELINA'),
];
