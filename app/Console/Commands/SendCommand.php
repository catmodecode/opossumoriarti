<?php

namespace App\Console\Commands;

use App\Services\TGBotService;
use Illuminate\Console\Command;

use function Laravel\Prompts\text;

class SendCommand extends Command
{

    private const SASHA = 'sasha';
    private const ELINA = 'elina';
    private array $chat = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->chat = [
            self::SASHA => config('users.sasha'),
            self::ELINA => config('users.elina'),
        ];
        /** @var TGBotService $tg */
        $tg = app()->make(TGBotService::class);
        while (true) {
            $message = text('Message:');
            $tg->send($this->chat[self::ELINA], $message);
            $this->line('Sent');
        }
    }
}
