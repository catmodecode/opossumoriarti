<?php

namespace App\Console\Commands;

use App\Services\TGBotService;
use Illuminate\Console\Command;
use Longman\TelegramBot\Entities\Update;

class ChatCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var TGBotService $tg */
        $tg = app()->make(TGBotService::class);
        while (true) {
            $response = $tg->receive()->getResult();
            array_walk($response, function (Update $update) {
                switch ($update->getUpdateType()) {
                    // case 'my_chat_member':
                    //     $this->group($update);
                    //     break;
                    case 'message':
                        $this->person($update);
                        break;
                    default:
                        $this->line($update->getUpdateType());
                        var_dump($update->getRawData());
                        $this->line('');
                };
                
            });
            sleep(1);
        }
    }

    public function person(Update $update): void
    {
        $message = $update->getMessage();
        
        $from = $message->getFrom();
        $name = $from->getUsername();
        
        $text = $message->getText();
        $chat = $message->getChat();
        
        $chatId = $chat->getId();
        $userId = $from->getId();
        
        $this->line("{$chatId}|{$name}|{$userId}: {$text}");
    }
}
