<?php

namespace App\Console\Commands;

use App\Services\TGBotService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Longman\TelegramBot\Entities\Update;
use Throwable;

class ResendCommand extends Command
{
    private const SASHA = 'sasha';
    private const ELINA = 'elina';
    private TGBotService $tg;

    private array $chat = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->chat = [
            self::SASHA => config('users.sasha'),
            self::ELINA => config('users.elina'),
        ];
        $this->tg = app()->make(TGBotService::class);
        while (true) {
            $response = $this->tg->receive()->getResult();
            if (!is_array($response)) {
                continue;
            }
            try {
            array_walk($response, function (Update $update) {
                switch ($update->getUpdateType()) {
                    case 'message':
                        $this->resend($update);
                        break;
                    default:
                        $this->line($update->getUpdateType());
                        var_dump($update->getRawData());
                        $this->line('');
                };
            });
            } catch (Throwable $e) {
                $this->tg->send($this->chat[self::SASHA], $e->getMessage());
            }
            sleep(1);
        }
    }

    public function resend(Update $update): void
    {
        $message = $update->getMessage();

        $from = $message->getFrom();
        $name = $from->getUsername();

        $text = $message->getText();
        $chat = $message->getChat();

        $chatId = $chat->getId();
        $userId = $from->getId();

        $this->line("{$chatId}|{$name}|{$userId}: {$text}");
        if (!in_array($userId, $this->chat)) {
            return;
        }

        $to = ($userId == $this->chat[self::SASHA])
            ? $this->chat[self::ELINA]
            : $this->chat[self::SASHA];
        $this->tg->resend($message, $to);
    }
}
