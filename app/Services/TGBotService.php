<?php

namespace App\Services;

use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

class TGBotService
{
    private Telegram $bot;
    public function __construct()
    {
        $this->bot = new Telegram(env('BOT_API_KEY'), env('BOT_USERNAME'));
        $this->bot->useGetUpdatesWithoutDatabase();
    }

    public function receive(): ServerResponse
    {
        return $this->bot->handleGetUpdates();
    }

    public function send(string $contact, string $data): void
    {
        Request::sendMessage([
            'chat_id' => $contact,
            'text' => $data,
        ]);
    }

    public function resend(Message $message, string $to) {
        $from = $message->getChat()->getId();
        $messageId = $message->getMessageId();
        Request::copyMessage([
            'from_chat_id' => $from,
            'message_id' => $messageId,
            'chat_id' => $to,
        ]);
    }

    public function invite(string $contact)
    {
        $this->bot->executeCommand('contacts.addContact');
        // return Request::createChatInviteLink([
        //     'chat_id' => $contact
        // ]);
    }
}